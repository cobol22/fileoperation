       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-GRADE1.
       AUTHOR. THANAWAT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
       
       DATA DIVISION.
       FILE SECTION.
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           05 STU-ID   PIC   X(8).
           05 MIDTERM-SCORE  PIC   9(2)V9(2).
           05 FINAL-SCORE  PIC   9(2)V9(2).
           05 PROJECT-SCORE  PIC   9(2)V9(2).
       PROCEDURE DIVISION.
       BEGIN.
           OPEN  OUTPUT SCORE-FILE
           MOVE "62160254" TO STU-ID 
           MOVE "34.05" TO MIDTERM-SCORE 
           MOVE "35.50" TO FINAL-SCORE
           MOVE "20" TO PROJECT-SCORE
           WRITE SCORE-DETAIL  
           

           MOVE "62160295" TO STU-ID 
           MOVE "40" TO MIDTERM-SCORE 
           MOVE "20" TO FINAL-SCORE
           MOVE "20" TO PROJECT-SCORE
           WRITE SCORE-DETAIL

           MOVE "62160140" TO STU-ID 
           MOVE "30.8" TO MIDTERM-SCORE 
           MOVE "40" TO FINAL-SCORE
           MOVE "20" TO PROJECT-SCORE
           WRITE SCORE-DETAIL

           MOVE "62160297" TO STU-ID 
           MOVE "40" TO MIDTERM-SCORE 
           MOVE "20" TO FINAL-SCORE
           MOVE "20" TO PROJECT-SCORE
           WRITE SCORE-DETAIL
           
           MOVE "62160274" TO STU-ID 
           MOVE "40" TO MIDTERM-SCORE 
           MOVE "40" TO FINAL-SCORE
           MOVE "20" TO PROJECT-SCORE
           WRITE SCORE-DETAIL
           CLOSE SCORE-FILE


           GOBACK 
           .
